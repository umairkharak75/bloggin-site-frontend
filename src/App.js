import React from 'react';
import Post from './posts/pages/Post';

import Drawer from '../src/shared/components/Drawer'
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch
} from 'react-router-dom';


import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact>
          <Drawer/>
        </Route>
        {/* <Route path="/places/new" exact>
          <NewPlace />
        </Route>
        <Redirect to="/" /> */}
      </Switch>
    </Router>
  );
}

export default App;
